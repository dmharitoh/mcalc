from mcalc import application
import logging
import pytest

num = 1
def test_converter_meters():
    logging.info("Trying to converte from m to mm")
    assert application(num, "m", "mm") == 1000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "m", "mm")))
    logging.info("Trying to converte from m to cm")
    assert application(num, "m", "cm") == 100
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "m", "cm")))
    logging.info("Trying to converte from m to dm")
    assert application(num, "m", "dm") == 10
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "m", "dm")))
    logging.info("Trying to converte from m to km")
    assert application(num, "m", "km") == 0.001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "m", "km")))
def test_converter_cm():
    logging.info("Trying to converte from cm to mm")
    assert application(num, "cm", "mm") == 10
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "cm", "mm")))
    logging.info("Trying to converte from cm to dm")
    assert application(num, "cm", "dm") == 0.1
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "cm", "dm")))
    logging.info("Trying to converte from cm to m")
    assert application(num, "cm", "m") == 0.01
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "cm", "m")))
    logging.info("Trying to converte from cm to km")
    assert application(num, "cm", "km") == 0.00001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "cm", "km")))

def test_converter_dm():
    logging.info("Trying to converte from dm to mm")
    assert application(num, "dm", "mm") == 100
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "dm", "mm")))
    logging.info("Trying to converte from dm to cm")
    assert application(num, "dm", "cm") == 10
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "dm", "cm")))
    logging.info("Trying to converte from dm to m")
    assert application(num, "dm", "m") == 0.1
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "dm", "m")))
    logging.info("Trying to converte from dm to km")
    assert application(num, "dm", "km") == 0.0001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "dm", "km")))

def test_converter_km():
    logging.info("Trying to converte from km to mm")
    assert application(num, "km", "mm") == 1000000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "km", "mm")))
    logging.info("Trying to converte from km to cm")
    assert application(num, "km", "cm") == 100000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "km", "cm")))
    logging.info("Trying to converte from km to m")
    assert application(num, "km", "m") == 1000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "km", "m")))
    logging.info("Trying to converte from km to dm")
    assert application(num, "km", "dm") == 10000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "km", "dm")))

def test_converter_mm():
    logging.info("Trying to converte from mm to cm")
    assert application(num, "mm", "cm") == 0.1
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "mm", "cm")))
    logging.info("Trying to converte from mm to dm")
    assert application(num, "mm", "dm") == 0.01
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "mm", "dm")))
    logging.info("Trying to converte from mm to m")
    assert application(num, "mm", "m") == 0.001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "mm", "m")))
    logging.info("Trying to converte from mm to km")
    assert application(num, "mm", "km") == 0.000001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(application(num, "mm", "km")))
logging.basicConfig(filename='test.txt', level=logging.INFO)
logging.info('start')
pytest.main(test_converter_mm())
pytest.main(test_converter_cm())
pytest.main(test_converter_dm())
pytest.main(test_converter_meters())
pytest.main(test_converter_km())
logging.info('done')

