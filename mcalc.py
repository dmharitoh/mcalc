import os

def startNumb(numb, s):
    if s == "mm":
        return numb
    elif s == "cm":
        return numb * 10
    elif s == "dm":
        return numb * 100
    elif s == "m":
        return numb * 1000
    elif s == "km":
        return numb * 1000000


def result(numb, s):
    if s == "mm":
        return numb
    elif s == "cm":
        return numb / 10
    elif s == "dm":
        return numb / 100
    elif s == "m":
        return numb / 1000
    elif s == "km":
        return numb / 1000000


def application(n, s, m):
    return result(startNumb(n, s), m)
